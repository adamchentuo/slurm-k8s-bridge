package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func label(pod *Pod, jobid string) error {
	annotations := map[string]string{
		"slurm.net/job_id": jobid,
	}
	patch := GenericPatch{
		Metadata{
			Annotations: annotations,
		},
	}

	var b []byte
	body := bytes.NewBuffer(b)
	err := json.NewEncoder(body).Encode(patch)
	if err != nil {
		fmt.Println(err)
		return err
	}

	url := "http://" + apiHost + podsPatchEndpoint + pod.Metadata.Name
	request, err := http.NewRequest("PATCH", url, body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	request.Header.Set("Content-Type", "application/strategic-merge-patch+json")
	request.Header.Set("Accept", "application/json, */*")

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		fmt.Println(err)
		return err
	}

	if resp.StatusCode != 200 {
		fmt.Println(err)
		return err
	}

	return nil
}
