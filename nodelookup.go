package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
)

// Translate a Slurm node name into K8s node.
// Matches against the slurm.net/nodename annotation,
// or falls back to the nodename
func nodelookup(lookup string) (Node, error) {
	var nodes NodeList

	request := &http.Request{
		Header: make(http.Header),
		Method: http.MethodGet,
		URL: &url.URL{
			Host:   apiHost,
			Path:   nodesEndpoint,
			Scheme: "http",
		},
	}
	request.Header.Set("Accept", "application/json, */*")

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		return Node{}, err
	}

	err = json.NewDecoder(resp.Body).Decode(&nodes)
	if err != nil {
		return Node{}, err
	}

	for _, node := range nodes.Items {
		nodename := node.Metadata.Annotations["slurm.net/nodename"]
		if nodename == lookup || node.Metadata.Name == lookup {
			return node, nil
		}
	}

	return Node{}, errors.New("Lookup failed")
}
